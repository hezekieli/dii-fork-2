﻿using UnityEngine;
using System.Collections;

public class QuickButtons : MonoBehaviour {

    public GameObject tutoManager;
    public GameObject playerBody;
    public GameObject lvl0Spawn;
    public GameObject lvl1Spawn;
    public GameObject lvl2Spawn;
    public GameObject lvl3Spawn;

    TutoManager tutoScript;


	// Use this for initialization
	void Start () {
        tutoScript = tutoManager.GetComponent<TutoManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("MenuGui"); //L lataa skenen
        if (Input.GetKeyUp("l")) Application.LoadLevel("Tutorrial3"); //tutoriaali uudestaan
        if (Input.GetKeyUp("s")) tutoScript.task = 1; //skipataan alkuintro
        if (Input.GetKeyUp("0")) playerBody.transform.position = lvl0Spawn.transform.position; //pelaaja hissille
        if (Input.GetKeyUp("1")) playerBody.transform.position = lvl1Spawn.transform.position; //pelaaja ekalle tasolle
        if (Input.GetKeyUp("2")) playerBody.transform.position = lvl2Spawn.transform.position; //pelaaja tokalle tasolle
        if (Input.GetKeyUp("3")) playerBody.transform.position = lvl3Spawn.transform.position; //pelaaja kolmannelle tasolle
        
	}
}
