﻿using UnityEngine;
using System.Collections;

public class Trigger4Manager : MonoBehaviour {
    public GameObject blackFader;
    public GameObject credits1;
    public GameObject credits2;

    BlackFader faderScript;
    BlackFader credits1Script;
    BlackFader credits2Script;

	// Use this for initialization
	void Start () 
    {
        faderScript = blackFader.GetComponent<BlackFader>();
        credits1Script = credits1.GetComponent<BlackFader>();
        credits1Script.fadeInSpeed = 0.4f;
        credits1Script.fadeOutSpeed = 1f;

        credits2Script = credits2.GetComponent<BlackFader>();
        credits2Script.fadeInSpeed = 0.4f;
        credits2Script.fadeOutSpeed = 0.4f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (faderScript.fadedToColor) credits1Script.fadeToColor = true;
        if (credits1Script.fadedToColor)
        {
            credits1Script.fadeToAlpha = true;
            credits2Script.fadeToColor = true; 
        }
        if (credits2Script.fadedToColor)
        {
            credits2Script.fadeToAlpha = true;
        }
        if (credits2Script.fadedToAlpha) Application.LoadLevel(2);
	}

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        faderScript.fadeToColor = true;
        Debug.Log("Kentän lopetus voidaan tehdä nyt"); //TODO kuuntelija että pelaaja osu ja verkon asettaminen alapuolelle
        //tutoScript.TriggerTest();
    }
}
