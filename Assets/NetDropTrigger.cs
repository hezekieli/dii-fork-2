﻿using UnityEngine;
using System.Collections;

public class NetDropTrigger : MonoBehaviour {

	public GameObject ringMaster;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
    
        if (collider.name == "Body")
        {
            Debug.Log(collider.name); //TODO kun pelaaja triggaa ni huohotusta ja tirehtöörin vapinaa
			ringMaster.GetComponent<RingmasterHandler>().PlayFalling();
        }

    }
 }

