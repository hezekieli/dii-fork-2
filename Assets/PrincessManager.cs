﻿using UnityEngine;
using System.Collections;

public class PrincessManager : MonoBehaviour {

	public string folder, folder1, folder2, folder3;
	private string path = "Sounds/SFX/Princess/";

	public AudioClip[] clips;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlaySaved(){

		int randomi = Random.Range (1, 3);

		if (randomi == 1) folder = folder1;
		if (randomi == 2) folder = folder2;
		if (randomi == 3) folder = folder3;

		Object[] clips_obj = Resources.LoadAll(path + folder);
		
		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];
			
			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError("Could not locate collection " + name );
		}
		
		audio.clip = clips [Random.Range(0, clips.Length)];
		if (!audio.isPlaying) audio.Play ();

	}
}
