﻿using UnityEngine;
using System.Collections;

using System.IO;

public class ItemDropManager : MonoBehaviour {

	public AudioClip drop;

	// Use this for initialization
	void Start () {
		audio.clip = drop;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.relativeVelocity.magnitude > 1f && !audio.isPlaying) {
			audio.volume = 0.1f*collision.relativeVelocity.magnitude;
			audio.Play();
		}
		
	}
}
