﻿using UnityEngine;
using System.Collections;

public class InfoTextTrigger : MonoBehaviour {

    public GameObject tutoManager;
    public Sprite infoText;

    TutoManager tutoScript;
    bool firstTime;
	// Use this for initialization
	void Start () {
        tutoScript = tutoManager.GetComponent<TutoManager>();
        firstTime = true;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (firstTime)
        {
            tutoScript.infoText.GetComponent<SpriteRenderer>().sprite = infoText;
            firstTime = false;
        }
    }
}
