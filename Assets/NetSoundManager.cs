﻿using UnityEngine;
using System.Collections;

public class NetSoundManager : MonoBehaviour {

	public AudioClip net_spread;
	public bool net1_spread;
	public bool net2_spread;

	public bool net1_spreaded;
	public bool net2_spreaded;

	// Use this for initialization
	void Start () {
		net1_spread = false;
		net2_spread = false;
		net1_spreaded = false;
		net2_spreaded = false;
		audio.clip = net_spread;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (net1_spread == true && net1_spreaded == false) SpreadNet1 ();
		if (net2_spread == true && net2_spreaded == false) SpreadNet2 ();
	
	}

	public void SpreadNet1(){
		audio.Play ();
		net1_spreaded = true;
	}

	public void SpreadNet2(){
		audio.Play ();
		net2_spreaded = true;
		
	}
}
