﻿using UnityEngine;
using System.Collections;

public class WalkControl : MonoBehaviour {

	public LayerMask LayerGround;
	public GameObject GroundCheck;
	public float GroundCheckRadius = 0.27f;

	public bool UseUnicycle;
	public GameObject Unicycle;
	private HingeJoint2D UnicycleJoint;

	public GameObject SpriteBodyLeft;
	public GameObject SpriteBodyRight;
	//SpriteRenderer spriteRenderer;
	public Sprite faceLeft;
	public Sprite faceRight;

	public bool firstStep; //if True, audio to be played is 1st step. if False, audio to be played is 2nd step
	public AudioClip footsteps1;
	public AudioClip footsteps2;

	public float walkSpeed = 1f;
	public float walkSpeedMax = 2f;

	// Use this for initialization
	void Start () {
		UnicycleJoint = Unicycle.GetComponent<HingeJoint2D>();

		//spriteRenderer = BodySpriteObject.GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update () {
	
		if (UseUnicycle) {

			JointMotor2D UnicycleMotor = UnicycleJoint.motor;

			// Going Right
			if (Input.GetAxis("Walk") < -0.5f) {
				UnicycleMotor.motorSpeed = 500f;
				SpriteBodyLeft.SetActive(false);
				SpriteBodyRight.SetActive(true);
				//spriteRenderer.sprite = faceRight;
			}
			// Going Left
			else if (Input.GetAxis("Walk") > 0.5f) {
				UnicycleMotor.motorSpeed = -500f;
				SpriteBodyLeft.SetActive(true);
				SpriteBodyRight.SetActive(false);
				//spriteRenderer.sprite = faceLeft;
			}
			else {
				UnicycleMotor.motorSpeed = 0f;
				if (rigidbody2D.velocity.x < -1) {
					SpriteBodyLeft.SetActive(true);
					SpriteBodyRight.SetActive(false);
				}
				if (rigidbody2D.velocity.x > 1) {
					SpriteBodyLeft.SetActive(false);
					SpriteBodyRight.SetActive(true);
				}
				//if (rigidbody2D.velocity.x < -1) spriteRenderer.sprite = faceLeft;
				//if (rigidbody2D.velocity.x > 1) spriteRenderer.sprite = faceRight;
			}
			UnicycleJoint.motor = UnicycleMotor;

			if (IsOnGround() && Mathf.Abs(UnicycleMotor.motorSpeed) > 0) {
				WalkSound();
			}
		
		}
		else {

			if (rigidbody2D.velocity.x < -1) {
				SpriteBodyLeft.SetActive(true);
				SpriteBodyRight.SetActive(false);
			}
			if (rigidbody2D.velocity.x > 1) {
				SpriteBodyLeft.SetActive(false);
				SpriteBodyRight.SetActive(true);
			}
			//if (rigidbody2D.velocity.x < -1) spriteRenderer.sprite = faceLeft;
			//if (rigidbody2D.velocity.x > 1) spriteRenderer.sprite = faceRight;

			if (IsOnGround()) {

				if (Input.GetAxis("Walk") < -0.5f)
				{
					Walk(true); //Right
					WalkSound();
					
				}

				if (Input.GetAxis("Walk") > 0.5f)
				{
					Walk(false); //Left
					WalkSound();
				}
			}
		}

	}

	bool IsOnGround()
	{
		if (Physics2D.OverlapCircle(GroundCheck.transform.position, GroundCheckRadius, LayerGround) != null) return true;
		return false;
	}

	void Walk(bool isRight)
	{
		float vX = rigidbody2D.velocity.x;
		if (isRight) 
		{
			// If the character isn't already moving at his max walking speed to the right.
			if (rigidbody2D.velocity.x < walkSpeedMax) 
			{
				if (rigidbody2D.velocity.x + walkSpeed > walkSpeedMax) vX = walkSpeedMax;
				else vX = rigidbody2D.velocity.x + walkSpeed;
			}
		}
		else 
		{
			// If the character isn't already moving at his max walking speed to the left
			if (rigidbody2D.velocity.x > -walkSpeedMax) 
			{
				if (rigidbody2D.velocity.x - walkSpeed < -walkSpeedMax) vX = -walkSpeedMax;
				else vX = rigidbody2D.velocity.x - walkSpeed;
			}
		}
		rigidbody2D.velocity = new Vector2 (vX, rigidbody2D.velocity.y);
	}

	void WalkSound()
	{
		if (!audio.isPlaying)
		{
			if (firstStep == true)
			{
				audio.clip = footsteps1;
				audio.Play();
				firstStep = false;
			}
			else
			{
				audio.clip = footsteps2;
				audio.Play();
				firstStep = true;
			}
		}
	}

}
