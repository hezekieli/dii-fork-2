using UnityEngine;
using System.Collections;

public class ArmDraw : MonoBehaviour {

	public bool IsTest = false;

	public GameObject Arm;
	public GameObject Hand;

	public bool UseTwoPieceArm;
	public GameObject SpriteWholeArm;
	public GameObject SpriteUpperArm;
	public GameObject SpriteForeArm;
	public GameObject SpriteHand;

	public float ArmReach = 1f;

	public float AngleDefault = 0;
	private float angleDefRadian = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		// Distance from Shoulder to Hand
		float dHand = Vector2.Distance (transform.position, Hand.transform.position);
		//if (IsTest) Debug.Log (dHand);

		//Debug.Log (distanceHand);
		if (UseTwoPieceArm) {

			// Angle of shoulder which is always the same as the Body. Body may in some cases/versions roll or the world may roll. Dunno...
			float angleShoulder = transform.eulerAngles.z / 180 * Mathf.PI;
			// Angle of hand. 
			float angleHand = -Mathf.Atan2 (Hand.transform.position.x - transform.position.x, Hand.transform.position.y - transform.position.y);
			// Must be modified according to shoulder/body rotation to move relatively to body.
			angleHand -= angleShoulder;
			//if (IsTest) Debug.Log(angleHand/Mathf.PI*180);
			// aTan2 gives values between -180 and 180. Need to modify the negative values to correct positive values.
			if (angleHand < 0) angleHand += 2*Mathf.PI;
			//if (IsTest) Debug.Log(angleHand);
			//if (IsTest) Debug.Log(angleHand/Mathf.PI*180);


			angleDefRadian = (((AngleDefault % 360) + 360) % 360) / 180 * Mathf.PI;
			//if (IsTest) Debug.Log(angleDefRadian);
			float shrinkAngleMin = angleDefRadian - (Mathf.PI/2);
			float shrinkAngleMax = angleDefRadian + (Mathf.PI/2);
			float elbowSwitchAngleMin = angleDefRadian;
			float elbowSwitchAngleMax = angleDefRadian + Mathf.PI;

			/*

			// If left Hand (IsRight = false)
			float handModulus = 1;	// 1 for left Hand, -1 for right Hand
			float angleDefault = 135;
			float shrinkAngleMin = Mathf.PI*1/4;
			float shrinkAngleMax = Mathf.PI*5/4;
			float elbowAngleMin = Mathf.PI*3/4;
			float elbowAngleMax = Mathf.PI*7/4;
			// If right Hand (IsRight = true)
			if (IsRightHand) {
				handModulus = -1;	// 1 for left Hand, -1 for right Hand
				angleDefault = 225;
				shrinkAngleMin = Mathf.PI*3/4;
				shrinkAngleMax = Mathf.PI*7/4;
				elbowAngleMin = Mathf.PI*1/4;
				elbowAngleMax = Mathf.PI*5/4;
			}
			*/

			// How much either piece of arm is shrinked
			float shrinkage = Mathf.Abs(Mathf.Cos(angleHand - angleDefRadian));	// Value between 0 and 1, where 0 is no shrinkage
			//if (IsTest) Debug.Log("Shrinkage = " + shrinkage);

			// How much the piece of arm needs to shrink, if shrinkage is full 1
			float needForShrink = Mathf.Clamp(((0.5f*ArmReach) - Mathf.Abs(dHand - (0.5f*ArmReach))), 0, 0.5f*ArmReach);
			//if (IsTest) Debug.Log(needForShrink);

			//float scaleForeArm = 1f;
			//float scaleUpperArm = 1f;
			float dForeArm = 0.5f * ArmReach;	// Distance from Hand to Elbow, ForeArm length
			float dUpperArm = 0.5f * ArmReach;	// Distance from Elbow to Shoulder, UpperArm length

			// Does the Upper- or the ForeArm shrink?
			float angleHandTemp = angleHand;	// For modifying the angleHand for comparison
			// Make sure the angleHand is on the same cycle of comparison values
			if (angleHand < shrinkAngleMin) angleHandTemp += 2 * Mathf.PI;
			else if (angleHand > shrinkAngleMax) angleHandTemp -= 2 * Mathf.PI;

			//if (IsTest) Debug.Log(shrinkAngleMin + " < " + angleHandTemp + " < " + shrinkAngleMax);

			// If ForeArm
			if (angleHandTemp > shrinkAngleMin && angleHandTemp < shrinkAngleMax) {
				//if (IsTest) Debug.Log("ForeArm shrinks");
				dForeArm -= shrinkage * needForShrink;
			}
			// If UpperArm
			else {
				//if (IsTest) Debug.Log("UpperArm shrinks");
				dUpperArm -= shrinkage * needForShrink;
			}

			SpriteForeArm.transform.localScale = new Vector3(1, 2*dForeArm/ArmReach, 1);
			SpriteUpperArm.transform.localScale = new Vector3(1, 2*dUpperArm/ArmReach, 1);



			// Angle in the triangle within Arm opposite to the ForeArm
			float t = Mathf.Clamp((dForeArm*dForeArm-dUpperArm*dUpperArm-dHand*dHand)/(-2*dUpperArm*dHand),-1, 1);		// t as in "temporary" :D
			//if (t > 1) t = 1;
			//else if (t < -1) t = -1;
			float aForeArm = Mathf.Acos(t);


			// Angle in the triangle within Arm opposite to the UpperArm
			t = Mathf.Clamp((dUpperArm*dUpperArm-dForeArm*dForeArm-dHand*dHand)/(-2*dForeArm*dHand),-1, 1);
			//if (t > 1) t = 1;
			//else if (t < -1) t = -1;
			float aUpperArm = Mathf.Acos(t);


			// Is the Elbow "below" or "above" shoulder?
			angleHandTemp = angleHand;
			if (angleHand < elbowSwitchAngleMin) angleHandTemp += 2 * Mathf.PI;
			else if (angleHand > elbowSwitchAngleMax) angleHandTemp -= 2 * Mathf.PI;

			// HACK-ratkasu: Jos kämmen aivan olkapään vieressä
			if (dHand < 0.01f) {

				SpriteUpperArm.transform.localEulerAngles = new Vector3(0, 0, AngleDefault);
				SpriteForeArm.transform.localEulerAngles = new Vector3(0, 0, AngleDefault + 180);
				SpriteHand.transform.eulerAngles = new Vector3(0, 0, SpriteForeArm.transform.eulerAngles.z);

				SpriteForeArm.transform.localPosition = new Vector2(dUpperArm * Mathf.Cos(angleDefRadian+(Mathf.PI/2)), dUpperArm * Mathf.Sin(angleDefRadian+(Mathf.PI/2)));

				//SpriteForeArm.transform.localPosition = new Vector3(0f, -0.25f, 0);
				//SpriteForeArm.transform.position = SpriteForeArm.transform.position + new Vector3(handModulus * 0.5f * Mathf.Sin(Mathf.PI*5/4), 0.5f * Mathf.Cos(Mathf.PI*5/4),0);
				//ForeArmSprite.transform.position = ForeArmSprite.transform.position + new Vector3(0.5f * Mathf.Sin((135 - Arm.transform.localEulerAngles.z)/180*Mathf.PI), 0.5f * Mathf.Cos((315 - Arm.transform.localEulerAngles.z)/180*Mathf.PI),0);
			}
			// If Below
			else if (angleHandTemp > elbowSwitchAngleMin && angleHandTemp < elbowSwitchAngleMax) {
				//if (IsTest) Debug.Log("Elbow Below");


				SpriteUpperArm.transform.localEulerAngles = new Vector3(0, 0, (angleHand - aForeArm) / Mathf.PI * 180);
				SpriteForeArm.transform.localEulerAngles = new Vector3(0, 0, (angleHand + aUpperArm) / Mathf.PI * 180);
				SpriteHand.transform.eulerAngles = new Vector3(0, 0, SpriteForeArm.transform.eulerAngles.z);
				//SpriteHand.transform.localEulerAngles = new Vector3(0, 0, -Arm.transform.localEulerAngles.z + (aUpperArm / Mathf.PI * 180));//(aUpperArm / Mathf.PI * 180));

				SpriteForeArm.transform.localPosition = new Vector2(-dUpperArm * Mathf.Sin(angleHand - aForeArm), dUpperArm * Mathf.Cos(angleHand - aForeArm));

			}
			// If Above
			else {
				//if (IsTest) Debug.Log("Elbow Above");


				SpriteUpperArm.transform.localEulerAngles = new Vector3(0, 0, (angleHand + aForeArm) / Mathf.PI * 180);
				SpriteForeArm.transform.localEulerAngles = new Vector3(0, 0, (angleHand - aUpperArm) / Mathf.PI * 180);
				SpriteHand.transform.eulerAngles = new Vector3(0, 0, SpriteForeArm.transform.eulerAngles.z);
				//SpriteHand.transform.localEulerAngles = new Vector3(0, 0, -Arm.transform.localEulerAngles.z - (aUpperArm / Mathf.PI * 180));//(-aUpperArm / Mathf.PI * 180));

				SpriteForeArm.transform.localPosition = new Vector2(-dUpperArm * Mathf.Sin(angleHand + aForeArm), dUpperArm * Mathf.Cos(angleHand + aForeArm));

			}

		

		}
		// Use simple stretching one-piece-arm
		else {
			SpriteWholeArm.transform.localScale = new Vector3(1, dHand, 1);
		}

		//ArmSprite.transform.localEulerAngles = new Vector3 (0, 0, angleHand);

	}
}
