﻿/* 
 * Place this script to the shoulder object of an arm.
 * The arm should have a GameObject with a HingeJoint2D and
 * a GameObject with SliderJoint2D.
 * 
 */

using UnityEngine;
using System.Collections;

public class ArmControl : MonoBehaviour {

	public bool IsTest;

	public LayerMask LayerAttachable;

	public GameObject Arm;
	public GameObject Hand;
	public GameObject InputCircle;
	public bool RelativeControls = false;

	public string ControlXAxis;
	public string ControlYAxis;
	public bool UseGrabButton = true;
	public string ControlGrabAxis;
	public string ControlGrabButton;

	public float GrabRadius = 0.25f;
	public float ArmReach = 1f;
	public float ArmRotateSpeedMax = 800f;
	public float ArmRotateForceMax = 1000f;
	public float HandSlideSpeedMax = 100f;
	public float HandSlideForceMax = 1000f;

	private HingeJoint2D ArmJoint;
	private SliderJoint2D HandJoint;
	private HingeJoint2D grabJoint;

	public AudioClip SoundGrab;

    public HingeJoint2D GrabJoint
    {
        get { return grabJoint; }
    }

	// Use this for initialization
	void Start () {
		ArmJoint = Arm.GetComponent<HingeJoint2D>();
		HandJoint = Hand.GetComponent<SliderJoint2D>();
		grabJoint = Hand.GetComponent<HingeJoint2D>();

		// SliderJoint's angle is relative to RigidBody's coordinate system and by doesn't react to transform.localEulerAngles changes by hand, only by RigidBody.
		// NOTE: This is needed only when the body is Initially rotated. Normally it shouldn't be. So, don't mind this. :)
		HandJoint.angle = transform.parent.eulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () {
	
		// If Grab button is pressed
		if (Input.GetKey(ControlGrabButton)) {
			// If there is nothing grabbed yet
			if (!GrabJoint.enabled) {

				// The Collider to attach to must be a circle collider
				Collider2D[] attachObjects = new Collider2D[10];
				//int numObj = Physics2D.OverlapPointNonAlloc(Hand.transform.position, attachObj, LayerAttachable);
				int numObj = Physics2D.OverlapCircleNonAlloc(Hand.transform.position, GrabRadius, attachObjects, LayerAttachable);
				for (int i = 0; i < numObj; i++) {
					if (attachObjects[i].rigidbody2D) {
						//Debug.Log (attachObj[i].transform.parent);

						Collider2D attachObj = attachObjects[i];
						PipeSliding pipe = attachObj.GetComponent<PipeSliding>();
						if (pipe) {
							attachObj = pipe.CheckAlternativeAttachObject(Hand).collider2D;
						}
						//if (check != null) attachObj = check.collider2D;//Debug.Log(check);

						if (attachObj is CircleCollider2D) {
							CircleCollider2D a = (CircleCollider2D)attachObj;
							GrabJoint.connectedAnchor = a.center;
						}
						else if (attachObj is BoxCollider2D) {
							BoxCollider2D a = (BoxCollider2D)attachObj;
							GrabJoint.connectedAnchor = a.center;
						}
						else GrabJoint.connectedAnchor = new Vector2(0, 0);

						GrabJoint.connectedBody = attachObj.rigidbody2D;
						GrabJoint.enabled = true;
						
						// See if the object has specific Grab Sound.
						Sounds soundsScript = attachObj.GetComponent<Sounds>();
						if (soundsScript) Hand.audio.clip = soundsScript.SoundGrab;
						else Hand.audio.clip = SoundGrab;
						Hand.audio.Play();
						break;
					}
				}

				/*
				Collider2D attachObj = Physics2D.OverlapCircle(Hand.transform.position, 0.4f, LayerAttachable);
				// If found object with a collider on right layer
				if (attachObj != null) {
					// If the object has rigidbody2D to connect to
					if (attachObj.rigidbody2D) {
						Debug.Log (attachObj.transform.parent);
						GrabJoint.connectedBody = attachObj.rigidbody2D;
						GrabJoint.enabled = true;

						// See if the object has specific Grab Sound.
						Sounds soundsScript = attachObj.GetComponent<Sounds>();
						if (soundsScript) Hand.audio.clip = soundsScript.SoundGrab;
						else Hand.audio.clip = SoundGrab;
						Hand.audio.Play();
					}
				}
				*/
			}
		}
		// If Grab button not pressed, disable HingeJoint and remove the connectedBody
		else {
			GrabJoint.enabled = false;
			GrabJoint.connectedBody = null;
		}


		JointMotor2D ArmMotor = ArmJoint.motor;
		JointMotor2D HandMotor = HandJoint.motor;

		// Calculate the target Circle according to Inputs
		// If using non-fixed-angle character body and want the inputs to be relative to bodys rotation
		if (RelativeControls) 
		{
			InputCircle.transform.localPosition = new Vector2 (ArmReach * Input.GetAxis (ControlXAxis), ArmReach * Input.GetAxis (ControlYAxis));
		}
		// If inputs relative to (non-rotating) camera
		else 
		{
			InputCircle.transform.localPosition = new Vector2 (0f, 0f);
			InputCircle.transform.position = new Vector2 (InputCircle.transform.position.x + ArmReach * Input.GetAxis (ControlXAxis), InputCircle.transform.position.y + ArmReach * Input.GetAxis (ControlYAxis));
		}
		//if (test) Debug.Log(Input.GetAxis(ControlYAxis));




		// Figure out where the circle is (distance from shoulder)
		float circleDistance = Vector2.Distance (transform.position, InputCircle.transform.position);

		// Figure out where the hand is (distance from shoulder)
		float handDistance = Vector2.Distance (transform.position, Hand.transform.position);
		
		// Figure out the difference
		float slideDistanceDifference = circleDistance - handDistance;

		// Distance from hand to circle
		float handCircleDistance = Vector2.Distance (InputCircle.transform.position, Hand.transform.position);

		// Calculate angle of the Circle relative to shoulder
		float circleAngle = Mathf.Atan2(InputCircle.transform.localPosition.y, InputCircle.transform.localPosition.x) / Mathf.PI * 180 + 270;

		// Angle between circle and arm rotation
		float armAngleDifference = Arm.transform.localEulerAngles.z - circleAngle;
		if (armAngleDifference < -180)	armAngleDifference += 360;
		else if (armAngleDifference > 180) armAngleDifference -= 360;
		// if (IsTest) Debug.Log(armAngleDifference);

		
		/* Dot product of vectors hand-shoulder and hand-inputCircle to determine if the handSlider should 
		 * extend or withdraw relative to shoulder */
		float dotPShoulderHandCircle = Vector2.Dot (
			new Vector2 (Arm.transform.parent.position.x - Hand.transform.position.x, Arm.transform.parent.position.y - Hand.transform.position.y), 
			new Vector2 (InputCircle.transform.position.x - Hand.transform.position.x, InputCircle.transform.position.y - Hand.transform.position.y));
		//if (test) Debug.Log (dotPShoulderHandCircle);

		// Relevant if abs(armAngleDifference) is less than 90
		float handDistanceOptimal = circleDistance * Mathf.Cos (Mathf.Abs(armAngleDifference)/180*Mathf.PI);
		if (IsTest && Mathf.Abs(armAngleDifference) < 90) {

			//Debug.Log(circleDistance + " * cos(" + Mathf.Abs(armAngleDifference)/180*Mathf.PI + ") = " + handDistanceOptimal);
			//Debug.Log(circleDistance);
			//Debug.Log(handDistanceOptimal);
		}


		/** Rotate Arm **/
		/** Slide Hand **/

		/* Set motorSpeeds accordingly	*/

		//motor.motorSpeed = ArmRotateSpeedFactor * armLAngleDifference;

		/*
		// If hand is free, no grab
		if (!GrabJoint.enabled) {
			HandMotor.maxMotorTorque = HandSlideForceMax;
			ArmMotor.maxMotorTorque = ArmRotateForceMax;


			//HandMotor.motorSpeed = slideDistanceDifference * 800f;
		}
		// If something is grabbed
		else {
			if (circleDistance < 0.05f) {
				HandMotor.maxMotorTorque = 100f;
				ArmMotor.maxMotorTorque = 100f;
			}
		}
		*/

		// If something is grabbed
		if (GrabJoint.enabled && circleDistance < 0.05f) {
			//HandMotor.maxMotorTorque -= 50f;
			HandMotor.maxMotorTorque = (HandMotor.maxMotorTorque > 10f * HandSlideForceMax * Time.deltaTime) ? HandMotor.maxMotorTorque - 10f * HandSlideForceMax * Time.deltaTime : 0f;
			//ArmMotor.maxMotorTorque -= 50f;
			ArmMotor.maxMotorTorque = (ArmMotor.maxMotorTorque > 10f * ArmRotateForceMax * Time.deltaTime) ? ArmMotor.maxMotorTorque - 10f * ArmRotateForceMax * Time.deltaTime : 0f;
		}
		// If hand is free, no grab
		else {
			HandMotor.maxMotorTorque = HandSlideForceMax;
			ArmMotor.maxMotorTorque = ArmRotateForceMax;
		}


		// If Arm almost correctly rotated
		if ((Mathf.Abs(armAngleDifference) < 2f) || circleDistance < 0.05f) {
			ArmMotor.motorSpeed = 0;
		}
		// If Arm getting close to target rotation
		else if (Mathf.Abs(armAngleDifference) < 30f) {
			ArmMotor.motorSpeed = armAngleDifference / 180 * ArmRotateSpeedMax;
			//ArmMotor.motorSpeed = (armAngleDifference >= 0f) ? ArmRotateSpeedMax : -ArmRotateSpeedMax;
		}
		else if (Mathf.Abs(armAngleDifference) < 90f) {
			//ArmMotor.motorSpeed = (armAngleDifference >= 0f) ? 0.01f * armAngleDifference * ArmRotateSpeedMax : 0.01f * armAngleDifference * ArmRotateSpeedMax;
			ArmMotor.motorSpeed = armAngleDifference / 180 * ArmRotateSpeedMax;
		}
		// If long way to rotate
		else {
			ArmMotor.motorSpeed = (armAngleDifference >= 0f) ? ArmRotateSpeedMax : -ArmRotateSpeedMax;
		}



		if (Mathf.Abs(armAngleDifference) < 90f) {
			HandMotor.motorSpeed = (handDistanceOptimal - handDistance) * HandSlideSpeedMax;
		}
		else {
			HandMotor.motorSpeed = -0.5f * HandSlideSpeedMax;
		}
		
		/*
		if (DotPShoulderHandCircle > 0) {
			HandMotor.motorSpeed = Mathf.Abs(slideDistanceDifference) * 200f;
		}
		else {
			HandMotor.motorSpeed = -200f * Mathf.Abs(slideDistanceDifference);
		}

		/*

		if (slideDistanceDifference > -0.03f && slideDistanceDifference < 0.03f) {
			HandMotor.motorSpeed = slideDistanceDifference;
		}
		else {
			HandMotor.motorSpeed = 50f * slideDistanceDifference;
			/*
			if (handCircleDistance > ArmReach && Mathf.Abs(armAngleDifference) > 90) {
				HandMotor.motorSpeed = -800f * slideDistanceDifference;
			}
			else {
				HandMotor.motorSpeed = 800f * slideDistanceDifference;
			}

		}
		*/

		ArmJoint.motor = ArmMotor;
		HandJoint.motor = HandMotor;

	}
}
