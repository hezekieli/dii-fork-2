﻿using UnityEngine;
using System.Collections;

public class CharacterControl : MonoBehaviour {

	public GameObject CircleL;		// Shows the D-Pad Input
	public GameObject CircleR;		// Shows the D-Pad Input
	public GameObject ArmL;
	public GameObject ArmR;
	public GameObject HandL;
	public GameObject HandR;

	public bool RelativeControls = false;

	public float ArmReach = 1f;
	public float ArmRotateSpeed = 800f;
	public float ArmRotateSpeedFactor = 8f;

	private HingeJoint2D ArmLHinge;
	private HingeJoint2D ArmRHinge;
	private SliderJoint2D HandLSlider;
	private SliderJoint2D HandRSlider;

	// Use this for initialization
	void Start () {
		ArmLHinge = ArmL.GetComponent<HingeJoint2D>();
		ArmRHinge = ArmR.GetComponent<HingeJoint2D>();
		HandLSlider = HandL.GetComponent<SliderJoint2D>();
		HandRSlider = HandR.GetComponent<SliderJoint2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
		// Calculate the target Circle according to Inputs
		// If using non-fixed-angle character body and want the inputs to be relative to it's rotation
		if (RelativeControls) 
		{
			CircleL.transform.localPosition = new Vector2 (ArmReach * Input.GetAxis ("HorizontalL"), ArmReach * Input.GetAxis ("VerticalL"));
			CircleR.transform.localPosition = new Vector2 (ArmReach * Input.GetAxis ("HorizontalR"), ArmReach * Input.GetAxis ("VerticalR"));
		}
		// If inputs relative to (non-rotating) camera
		else 
		{
			CircleL.transform.localPosition = new Vector2 (0f, 0f);
			//CircleL.transform.localPosition = new Vector2 (-1.25f, 1f);
			CircleL.transform.position = new Vector2 (CircleL.transform.position.x + ArmReach * Input.GetAxis ("HorizontalL"), CircleL.transform.position.y + ArmReach * Input.GetAxis ("VerticalL"));
			//CircleL.transform.localPosition = new Vector2 (ArmReach * Input.GetAxis ("HorizontalL"), ArmReach * Input.GetAxis ("VerticalL"));


			CircleR.transform.localPosition = new Vector2 (0f, 0f);
			//CircleR.transform.localPosition = new Vector2 (1.25f, 1f);
			CircleR.transform.position = new Vector2 (CircleR.transform.position.x + ArmReach * Input.GetAxis ("HorizontalR"), CircleR.transform.position.y + ArmReach * Input.GetAxis ("VerticalR"));
			//CircleR.transform.localPosition = new Vector2 (ArmReach * Input.GetAxis ("HorizontalR"), ArmReach * Input.GetAxis ("VerticalR"));
		}

		/** Rotate Arm Left **/

		// Find out which way and how much to turn the arm.
		// Angle between circle and arm rotation
		float circleLAngle = Mathf.Atan2(CircleL.transform.localPosition.y, CircleL.transform.localPosition.x) / Mathf.PI * 180 + 90;

		float armLAngleDifference = ArmL.transform.localEulerAngles.z - circleLAngle;
		if (armLAngleDifference < -180)	armLAngleDifference += 360;
		if (armLAngleDifference > 180) armLAngleDifference -= 360;
		//Debug.Log (armLAngleDifference);

		/* Dot product of vectors hand-shoulder and hand-inputCircle to determine if the handSlider should 
		 * extend or withdraw relative to shoulder */
		float DotPShoulderHandCircle = Vector2.Dot (
			new Vector2 (ArmL.transform.parent.position.x - HandL.transform.position.x, ArmL.transform.parent.position.y - HandL.transform.position.y), 
			new Vector2 (CircleL.transform.position.x - HandL.transform.position.x, CircleL.transform.position.y - HandL.transform.position.y));
		Debug.Log (DotPShoulderHandCircle);

		float circleLDistance = Vector2.Distance (new Vector2 (0f, 0f), new Vector2 (CircleL.transform.localPosition.x, CircleL.transform.localPosition.y));


		// Set motorSpeed accordingly
		JointMotor2D ArmLMotor = ArmLHinge.motor;

		if ((armLAngleDifference > -2f && armLAngleDifference < 2f) || circleLDistance < 0.1f) {
			ArmLMotor.motorSpeed = 0;
		}
		else if (armLAngleDifference > -30f && armLAngleDifference < 30f) 
			ArmLMotor.motorSpeed = ArmRotateSpeedFactor * armLAngleDifference;
		else 
			ArmLMotor.motorSpeed = (armLAngleDifference >= 0f) ? ArmRotateSpeed : -ArmRotateSpeed;

		//motor.motorSpeed = ArmRotateSpeedFactor * armLAngleDifference;

		ArmLHinge.motor = ArmLMotor;
		/*
		if ((armLAngleDifference > -5f && armLAngleDifference < 5f) || circleLDistance < 0.1f) {
			//Debug.Log (circleLDistance);
			ArmLHinge.useMotor = false;
		}
		else {
			ArmLHinge.useMotor = true;
		}
		*/


		/** Slide Hand Left **/

		// Figure out where the circle is (distance from shoulder)

		// Figure out where the hand is (distance from shoulder)
		//float handLDistance = Vector2.Distance (new Vector2 (ArmL.transform.parent.position.x, ArmL.transform.parent.position.y), new Vector2 (HandL.transform.position.x, HandL.transform.position.y));
		float handLDistance = 0.5f - HandL.transform.localPosition.y;
		//Debug.Log (handLDistance + " tai helpommin: " + (0.5f - HandL.transform.localPosition.y));
		// Figure out the difference
		float handLDistanceDifference = handLDistance - circleLDistance;
		//Debug.Log (handLDistanceDifference);
		// Set motorSpeed accordingly
		JointMotor2D HandLMotor = HandLSlider.motor;

		HandLMotor.motorSpeed = 800f * handLDistanceDifference;

		if (handLDistanceDifference > -0.03 && handLDistanceDifference < 0.03) {
			HandLMotor.motorSpeed = 0;
		}

		HandLSlider.motor = HandLMotor;

		//Debug.Log("VerticalL = " + Input.GetAxis("VerticalL"));



		if (circleLDistance < 0.1) {
			ArmLHinge.useMotor = false;
			HandLSlider.useMotor = false;
		}
		else {
			ArmLHinge.useMotor = true;
			HandLSlider.useMotor = true;
		}









		// Force Hand towards Circle controlled by Input
		//HandL.rigidbody2D.AddForce(new Vector2 (Mathf.Abs(Input.GetAxis ("HorizontalL")) * HandForce * (CircleL.transform.position.x - HandL.transform.position.x), Mathf.Abs(Input.GetAxis ("VerticalL")) * HandForce * (CircleL.transform.position.y - HandL.transform.position.y)));
		//HandR.rigidbody2D.AddForce(new Vector2 (Mathf.Abs(Input.GetAxis ("HorizontalR")) * HandForce * (CircleR.transform.position.x - HandR.transform.position.x), Mathf.Abs(Input.GetAxis ("VerticalR")) * HandForce * (CircleR.transform.position.y - HandR.transform.position.y)));

		//HandL.rigidbody2D.AddForce(new Vector2 (HandForce * (CircleL.transform.position.x - HandL.transform.position.x), HandForce * (CircleL.transform.position.y - HandL.transform.position.y)));
		//HandR.rigidbody2D.AddForce(new Vector2 (HandForce * (CircleR.transform.position.x - HandR.transform.position.x), HandForce * (CircleR.transform.position.y - HandR.transform.position.y)));

		// Hover Mode :D
		//HandL.rigidbody2D.AddForce(new Vector2(Input.GetAxis ("HorizontalL") * HandForce, Input.GetAxis ("VerticalL") * HandForce));
		//HandR.rigidbody2D.AddForce(new Vector2(Input.GetAxis ("HorizontalR") * HandForce, Input.GetAxis ("VerticalR") * HandForce));
	}
}
