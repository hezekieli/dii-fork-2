﻿using UnityEngine;
using System.Collections;

public class Crank : MonoBehaviour {

	public float valueStart = 0f;
	public float valueMin = 0f;
	public float valueMax = 360f;
	private float valueCurrent;
	private float valuePrevious;
	private float valueProgress;

	public GameObject Lift;
	private CrankLift crankLift;

	public float ValueProgress {
		get { return valueProgress; }
	}

	private HingeJoint2D CrankJoint;

	// Use this for initialization
	void Start () {
		valuePrevious = transform.localEulerAngles.z;
		valueCurrent = valuePrevious;
		valueProgress = valueStart;

		CrankJoint = GetComponent<HingeJoint2D> ();
		if (Lift) crankLift = Lift.GetComponent<CrankLift> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (crankLift) valueProgress = valueMin + crankLift.ElevationTarget * (valueMax - valueMin);

		valuePrevious = valueCurrent;
		valueCurrent = transform.localEulerAngles.z;

		float valueChange = valuePrevious - valueCurrent;
		//Debug.Log (valueChange);
		if (valueChange > 180f) valueChange -= 360f;
		else if (valueChange < -180f) valueChange += 360f;

		//Debug.Log (valueCurrent + " - " + valuePrevious + " = " + valueChange);

		valueProgress += valueChange;

		if (valueProgress < valueMin) valueProgress = valueMin;
		else if (valueProgress > valueMax) valueProgress = valueMax;
		//Debug.Log (valueProgress);

		if (crankLift) crankLift.ElevationTarget = (valueProgress - valueMin) / (valueMax - valueMin);


		/*
		if (valueProgress - 90f < valueMin) {
			//Debug.Log("Limited by Min");
			JointAngleLimits2D limits = CrankJoint.limits;
			//limits.min = (valueMin + 360f) % 360f;
			//limits.max = valueMin + 315f;
			CrankJoint.limits = limits;
			//CrankJoint.useLimits = true;
		}
		else if (valueProgress + 90f > valueMax) {
			//Debug.Log("Limited by Max");
			JointAngleLimits2D limits = CrankJoint.limits;
			//limits.min = valueMax - 315f;
			//limits.max = (valueMax + 360f) % 360f;
			CrankJoint.limits = limits;
			//CrankJoint.useLimits = true;
		}
		else {
			CrankJoint.useLimits = false;
		}
		CrankJoint.useLimits = false;
		*/

	}
}
