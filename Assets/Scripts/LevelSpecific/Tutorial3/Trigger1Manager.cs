﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Triggeri joka laukaistaa kun pelaaja on ensimmäisellä tasolla kiipeilemässä.
/// Kiipeilynarut pois ja verkot tilalle.
/// </summary>
public class Trigger1Manager : MonoBehaviour {

    public GameObject tutoManager;
    public GameObject[] nets;   //nämä otetaan käyttöön
    public GameObject[] ropes; //nämä poistetaan käytöstä

	public GameObject netSound;
	public GameObject music;

    TutoManager tutoScript;
    bool firstCollide;
	// Use this for initialization
	void Start () {
        firstCollide = true;
        tutoScript = tutoManager.GetComponent<TutoManager>();
        foreach (GameObject net in nets)
        {
            net.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.name); //TODO kuuntelija että pelaaja osu ja verkon asettaminen alapuolelle
        if (firstCollide)
        {
			netSound.GetComponent<NetSoundManager>().net1_spread = true;
			music.GetComponent<musicHandler>().nykyneVaihe = 2;
            /*
            foreach (GameObject rope in ropes)
            {
                rope.SetActive(false);
            }
            */
            foreach (GameObject net in nets)
            {
                net.SetActive(true);
            }
        }
        //tutoScript.TriggerTest();
    }
}
