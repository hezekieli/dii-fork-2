﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Triggeri joka laukaistaa kun pelaaja on toisella tasolla kiipeilemässä.
/// Kiipeilynarut pois ja verkot tilalle.
/// </summary>
public class Trigger2Manager : MonoBehaviour {

    public GameObject tutoManager;
    public GameObject weight1; //disabloidaan painot alhaalta ettei ne laukase mitään triggereitä tms
    public GameObject weight2;

	public GameObject netSound;

    public GameObject[] nets;   //nämä otetaan käyttöön
    public GameObject[] ropes; //nämä poistetaan käytöstä
    TutoManager tutoScript;
    bool firstCollide;

    // Use this for initialization
    void Start()
    {
        firstCollide = true;
        tutoScript = tutoManager.GetComponent<TutoManager>();
        foreach (GameObject net in nets)
        {
            net.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.name); //TODO kuuntelija että pelaaja osu ja verkon asettaminen alapuolelle
        if (firstCollide)
        {
			netSound.GetComponent<NetSoundManager>().net2_spread = true;
            /*
            foreach (GameObject rope in ropes)
            {
                rope.SetActive(false);
            }
            */
            foreach (GameObject net in nets)
            {
                net.SetActive(true);
            }

			if (weight1 && weight2) {
            	weight1.SetActive(false);
            	weight2.SetActive(false);
			}
        }
        //tutoScript.TriggerTest();
    }
}

