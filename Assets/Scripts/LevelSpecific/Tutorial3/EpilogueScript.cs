﻿using UnityEngine;
using System.Collections;

public class EpilogueScript : MonoBehaviour {

    public GameObject lightSwitch;
    Cord cordScript;

	// Use this for initialization
	void Start () 
    {
        cordScript = lightSwitch.GetComponent<Cord>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (cordScript.Activated) renderer.material.color = Color.white;
        if (Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("MenuGui");
	}
}
