﻿	using UnityEngine;
using System.Collections;

public class TutoManager : MonoBehaviour {

    //Kaikki skenen objektit joita tarvitaan scriptissä
    public GameObject[] handPoints;
    public GameObject[] handPointParticles;
    public GameObject[] weightPoints;
    public GameObject[] weightPointParticles;
    public GameObject rightHand;
    public GameObject leftHand;
    public GameObject weight1;
    public GameObject weight2;
    public GameObject charShoulderLeft;
    public GameObject charShoulderRight;
    public GameObject weightCheckPoint;
    public GameObject infoText;
    public GameObject beginRoomCollider;

	public GameObject ringMasterManager;
	public GameObject music;

    public Sprite moveHands;
    public Sprite liftHands;
    public Sprite grabObjects;
    public Sprite walk;
    public Sprite weightsUp;
	public GameObject Lift;

	private CrankLift crankLift;
    
    //public Sprite none;

    //Muut muuttujien alustukset
    public int task = 0;

    SpriteRenderer infoRenderer;

    ArmControl armControlLeft;
    ArmControl armControlRight;

    bool pointIsVisible = false;
    int pointCalculator;

    float taskTimer;
    bool rHandCloseEnough = false;
    bool lHandCloseEnough = false;

    bool lastHackBool = true; //yhessä ohjetekstissä tarvitaan

    bool addForceToWeight = true;
    Vector2 forceToWeight1 = new Vector2(25000, 35000); //TODO voimat pitää tarkastaa kun verhot tulee skeneen
    Vector2 forceToWeight2 = new Vector2(25000, 30000);


	// Use this for initialization
	void Start () {
        task = 0;
        pointCalculator = 0;
        armControlLeft = charShoulderLeft.GetComponent<ArmControl>();
        armControlRight = charShoulderRight.GetComponent<ArmControl>();
        weight1.SetActive(false);
        weight2.SetActive(false);
        bool lastHackBool = true;

        infoRenderer = infoText.GetComponent<SpriteRenderer>();

        //Poistetaan aluksi kaikki pisteet ja partikkelit pois näkyvistä, eli disabloidaan
        foreach (GameObject point in handPoints)
        {
            point.renderer.enabled = false;
        }
        foreach (GameObject weightPoint in weightPoints)
        {
            weightPoint.renderer.enabled = false;
        }

		if (Lift) crankLift = Lift.GetComponent<CrankLift>();
	}
	

	// Update is called once per frame
	void Update ()
    {
        switch (task)
        {
            //Aloitetaan tirehtöörin puheella
            case 0:
                //TODO tässä keississä oltas nii kaua ku tirehtöörin alkupuhe kestää... hoida juuso taski seuraavaan (case1) kun tirehööri on puhunut.
                break;

            case 1:
                infoRenderer.sprite = moveHands;
                beginRoomCollider.SetActive(false);
				if (music.GetComponent<musicHandler>().playedIntro == false){
					music.GetComponent<musicHandler>().audio.Stop();
				}
                task++;
                //TODO tirehtöörin alkupuhe, aputeksti pelaajalle kun se on odotushuoneessa, odotushuoneen oven tms. avaaminen kun puhe on pidetty
               
                break;
            //Voimamiehen posetus
            case 2:
                if (lastHackBool)
                {
                    infoRenderer.sprite = walk;
                    lastHackBool = false;
                }
                MoveHands();
                break;

            //Ensimmäinen paino ja voimamiehen voimien esittely
            case 3:
				if (ringMasterManager.GetComponent<RingmasterHandler>().strength == false){
					ringMasterManager.GetComponent<RingmasterHandler>().PlayStrength();
				}
                infoRenderer.sprite = grabObjects;
                WeightObservator(weight1, forceToWeight1, 0);
                break;

            //Toinen paino heitetään verhojen takaa. Toista painoa ei nosteta yksin ilmaan vaan se nostetaan ekan painon kanssa yhdessä
            case 4:
                infoRenderer.sprite = weightsUp;
                if (addForceToWeight)
                {
                    weight2.SetActive(true);
                    weight2.rigidbody2D.AddForceAtPosition(forceToWeight2, weight2.transform.position);
                    addForceToWeight = false;
                    weightPoints[1].renderer.enabled = true;
                    weightPoints[2].renderer.enabled = true;
                    task++;
                }
                //WeightObservator(weight2, forceToWeight2, 1); 
                break;

            //Molemmat painot ja voimamiehen voimien esittely
            case 5:
                WeightsInHandsObservator(1,2);
                break;

            //Ringmasterin välipölinät ja kiipeilyn aloittaminen
            case 6:
                //infoRenderer.sprite = none; pitää asettaa tyhjä
				if (ringMasterManager.GetComponent<RingmasterHandler>().kiipeily == false){
					ringMasterManager.GetComponent<RingmasterHandler>().PlayClimb();
				}
                //TODO kamera ulos ja tirehtöörin välipuhe neidon pelastamiseen
                break;
        }
	
	}


    /// <summary>
    /// Observes that both two weights are in hands and high enough
    /// </summary>
    private void WeightsInHandsObservator(int weightPoint1, int weightPoint2)
    {
        
        //jos molemmissa käsissä objekti
        //if(weightPoints[]
        if (armControlLeft.GrabJoint.connectedBody && armControlRight.GrabJoint.connectedBody)
        {
            //jos molemissa käsissä eri objekti
            if (armControlLeft.GrabJoint.connectedBody.gameObject != armControlRight.GrabJoint.connectedBody.gameObject)  //(weight1.transform.position.y > weightCheckPoint.transform.position.y && weight2.transform.position.y > weightCheckPoint.transform.position.y)
            {
                //Debug.Log(weight1.transform.position + " | | " + weightPoints[weightPoint1].transform.position);
                //jos painot ovat tarpeeksi korkealla
                if (CloseEnough(weight1.transform.position, weightPoints[weightPoint1].transform.position, 0.8f) ||
                    CloseEnough(weight2.transform.position, weightPoints[weightPoint1].transform.position, 0.8f))
                {
                    
                    if (weightPoints[weightPoint1].renderer.material.color != Color.green)
                    {
                        weightPoints[weightPoint1].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
                    }
                }
                else weightPoints[weightPoint1].renderer.material.color = Color.white;


                if (CloseEnough(weight1.transform.position, weightPoints[weightPoint2].transform.position, 0.8f) ||
                   CloseEnough(weight2.transform.position, weightPoints[weightPoint2].transform.position, 0.8f))
                {
                    if (weightPoints[weightPoint2].renderer.material.color != Color.green)
                    {
                        weightPoints[weightPoint2].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
                    }
                }
                else weightPoints[weightPoint2].renderer.material.color = Color.white;


                if (weightPoints[weightPoint1].renderer.material.color == Color.green && weightPoints[weightPoint2].renderer.material.color == Color.green)
                {
                    if (taskTimer == float.MaxValue) taskTimer = Time.time;
                    if (taskTimer + 1f < Time.time)
                    {
                        //addForceToWeight = true; //TODO tähän yleisön hurraus
                        taskTimer = float.MaxValue;
                        weightPoints[weightPoint1].renderer.enabled = false;
                        weightPoints[weightPoint2].renderer.enabled = false;
                        weightPointParticles[weightPoint1].SetActive(true);
                        weightPointParticles[weightPoint2].SetActive(true);
                        infoRenderer.sprite = walk;
                        task++;
                    }
                }
                
                
            }
        }

        else taskTimer = float.MaxValue;
    }


    /// <summary>
    /// Shoots the weights from behind left courtain if must. Observes that player
    /// keeps weight in air over two seconds. If keeps, mission is increased by one. 
    /// </summary>
    /// <param name="weight">weight</param>
    private void WeightObservator(GameObject weight, Vector2 force, int weightPoint)
    {
        weight.SetActive(true);
        if (addForceToWeight)
        {
            weight.rigidbody2D.AddForceAtPosition(force, weight.transform.position);
            addForceToWeight = false;
            weightPoints[weightPoint].renderer.enabled = true;
        }
        //jos toisessa kädessä joku objekti
        if ((armControlLeft.GrabJoint.connectedBody || armControlRight.GrabJoint.connectedBody))// == weight &&
        {
            //Jos paino on tarpeeksi ylhäällä
            if (CloseEnough(weight.transform.position, weightPoints[weightPoint].transform.position, 0.8f)) //(weight.transform.position.y > weightCheckPoint.transform.position.y)
            {
                if (weightPoints[weightPoint].renderer.material.color != Color.green)
                {
                    weightPoints[weightPoint].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
                }

                if (taskTimer == float.MaxValue) taskTimer = Time.time;
                if (taskTimer + 1f < Time.time)
                {
                    addForceToWeight = true; //TODO tähän yleisön hurraus, hämmennys tms
                    taskTimer = float.MaxValue;
                    weightPoints[weightPoint].renderer.enabled = false;
                    weightPointParticles[weightPoint].SetActive(true);
                    task++;
                }
            }
            else
            {
                taskTimer = float.MaxValue;
                weightPoints[weightPoint].renderer.material.color = Color.white;
            }
        }         
    }


    /// <summary>
    /// Player must move hands to specific points.
    /// </summary>
    void MoveHands()
    {
        switch (pointCalculator)
        {
            case 0:
                if (!pointIsVisible) ObservePoint();
                break;

            case 1:
                if (!pointIsVisible) ObservePoint();
                break;

            case 2:
                if (!pointIsVisible) ObservePoint();
                break;

            case 3:
                if (!pointIsVisible) ObservePoint();
                break;

            //kahden pisteen metästys
            case 4:
                if (!pointIsVisible) ObserveTwoPoints();
                break;
            
            //Hypätään yksi yli koska aloitettiin tarkastelemaan kahta pistettä
            case 6:
                if (!pointIsVisible) ObserveTwoPoints();
                break;

            case 8:
                task++;
                break;
        }
    }


    /// <summary>
    /// Makes points visible and checks if player keeps both hands close enough
    /// points. Same as ObservePoint- function but with two points to observe. 
    /// </summary>
    private void ObserveTwoPoints()
    {
        handPoints[pointCalculator].renderer.enabled = true;
        handPoints[pointCalculator + 1].renderer.enabled = true;
            //jos oikea käsi on tarpeeks lähellä                                                                      //Jos vasen tarpeeks lähellä
        if (CloseEnough(handPoints[pointCalculator].transform.position, rightHand.transform.position, 0.3f) || CloseEnough(handPoints[pointCalculator].transform.position, leftHand.transform.position, 0.3f))
        {
            if (handPoints[pointCalculator].renderer.material.color != Color.green)
            {
                handPoints[pointCalculator].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
            }

        }
        else
        {
            handPoints[pointCalculator].renderer.material.color = Color.white;
        }

        if (CloseEnough(handPoints[pointCalculator + 1].transform.position, rightHand.transform.position, 0.3f) || CloseEnough(handPoints[pointCalculator + 1].transform.position, leftHand.transform.position, 0.3f))
        {
            if (handPoints[pointCalculator + 1].renderer.material.color != Color.green)
            {
                handPoints[pointCalculator + 1].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
            }

        }
        else
        {
            handPoints[pointCalculator + 1].renderer.material.color = Color.white;
        }

        //Jos molemmat pisteet vihreitä ja molemmat kädet siis tarpeeks lähellä eri pisteitä
        if (handPoints[pointCalculator].renderer.material.color == Color.green && handPoints[pointCalculator + 1].renderer.material.color == Color.green)
        {
            if (taskTimer == float.MaxValue) taskTimer = Time.time;
            if (taskTimer + 1f < Time.time)
            {
                handPoints[pointCalculator].renderer.enabled = false;
                handPoints[pointCalculator + 1].renderer.enabled = false;
                handPointParticles[pointCalculator].SetActive(true);
                handPointParticles[pointCalculator + 1].SetActive(true);
                pointIsVisible = false;
                pointCalculator += 2;
                taskTimer = float.MaxValue;
            }
        }
        //string piste = handPoints[pointCalculator + 1].name;
    }


    /// <summary>
    /// Makes point visible and chekcs if player keeps hand close enough the red circle.
    /// Player must also keep hand close enough the circle at least 0.5 seconds. 
    /// </summary>
    private void ObservePoint()
    {
        handPoints[pointCalculator].renderer.enabled = true;
        rHandCloseEnough = CloseEnough(handPoints[pointCalculator].transform.position, rightHand.transform.position, 0.3f);
        lHandCloseEnough = CloseEnough(handPoints[pointCalculator].transform.position, leftHand.transform.position, 0.3f);
        if (rHandCloseEnough || lHandCloseEnough)
        {
            handPoints[pointCalculator].renderer.material.color = Color.Lerp(Color.white, Color.green, Time.time);
            if (taskTimer == float.MaxValue) taskTimer = Time.time;
            if (taskTimer + 1f < Time.time)
            {
                handPoints[pointCalculator].renderer.enabled = false;
                handPointParticles[pointCalculator].SetActive(true); //TODO jos kerkiää ni vois tehä hienomman partikkelin :D
                pointIsVisible = false;
                pointCalculator++;
                taskTimer = float.MaxValue;
                //TODO juuso, tähä vois laittaa jonku onnistumisäänen kun piste on napattu ja partikkeli toistetaan
            }
        }
        else
        {
            taskTimer = float.MaxValue;
            handPoints[pointCalculator].renderer.material.color = Color.white;
        }
    }


    /// <summary>
    /// Checks if two vectors are close enough
    /// </summary>
    /// <param name="firstvector">vector one</param>
    /// <param name="secondvector">vector two</param>
    /// <param name="maxdistance">max distance between two vectors</param>
    /// <returns>true if vectors are close enough, otherwice false</returns>
    private bool CloseEnough(Vector3 firstvector, Vector3 secondvector, float maxdistance)
    {
        if (Vector2.Distance(firstvector, secondvector) < maxdistance) return true;
        return false;
    }

    /// <summary>
    /// Testataan triggeriä
    /// </summary>
    public void TriggerTest()
    {
        Debug.Log("homo");
    }
}
