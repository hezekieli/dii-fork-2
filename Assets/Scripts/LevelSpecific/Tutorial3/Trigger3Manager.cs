﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Pelaaja on prinsessatornissa ja nyt valmistellaan kaikki tapahtumat kentän lopettamiseksi
/// </summary>
public class Trigger3Manager : MonoBehaviour {

    public GameObject tutoManager;
    public GameObject endLvlTrigger;
	public GameObject princess;

    TutoManager tutoScript;
    // Use this for initialization
    void Start()
    {
        tutoScript = tutoManager.GetComponent<TutoManager>();
        endLvlTrigger.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Triggeri joka laukeaa kun pelaaja siihen osuu
    /// </summary>
    /// <param name="collider">Törmääjä</param>
    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("lvl 3 collider"); //TODO kuuntelija että pelaaja osu ja verkon asettaminen alapuolelle
        endLvlTrigger.SetActive(true);
		princess.GetComponent<PrincessManager> ().PlaySaved ();
        //tutoScript.TriggerTest();
    }
}
