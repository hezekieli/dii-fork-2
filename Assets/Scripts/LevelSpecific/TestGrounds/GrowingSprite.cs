﻿using UnityEngine;
using System.Collections;

public class GrowingSprite : MonoBehaviour {
    public GameObject parent;
    public GameObject crank;
    CranckPickUp crankScript;
	// Use this for initialization
	void Start () 
    {
        
        crankScript = crank.GetComponent<CranckPickUp>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        parent.transform.localScale = new Vector3(parent.transform.localScale.x,1 + crankScript.GetProgress() * 25, parent.transform.localScale.z);
        Debug.Log("Crank prog in growing sprite: " + crankScript.GetProgress());
	}
}
