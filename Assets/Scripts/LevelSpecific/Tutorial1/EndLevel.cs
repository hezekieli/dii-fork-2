﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {

    public GameObject manager;

    TutorialManager managerScript;
	// Use this for initialization
	void Start () {
        managerScript = manager.GetComponent<TutorialManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        managerScript.endLevel();
    }

    
}
