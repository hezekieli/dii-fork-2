﻿using UnityEngine;
using System.Collections;

public class RingmasterHandler : MonoBehaviour {
	public int interval;
	public int maxRand;
	public float timer;
	public string folder, folder1, folder2, folder3, folder4, folder5, folder6;
	private string path = "Sounds/SFX/Ringmaster/Filling/";
	public int rand;

	public AudioClip showStrength;
	public bool strength = false;
	public AudioClip kiipeemineAlkaa;
	public bool kiipeily = false;
	private bool kiipeilyPlayed = false;

	public AudioClip endingSpeak;
	public bool ending;
	public bool endingdone;

	private string previous;

	private string[] pathsArray;
	public int stage;
	public AudioClip[] clips;

	// Use this for initialization
	void Start () {
		stage = 0;
		timer = 0;
		rand = Random.Range (0, maxRand);
		ending = false;
		endingdone = false;

	
	}
	
	// Update is called once per frame
	void Update () {

		if (!audio.isPlaying && kiipeily == true && kiipeilyPlayed == false){
			stage = 1;
			kiipeilyPlayed = true;
		}

		/* As soon as Stage changes from initial 0
		 * to 1, timer starts to run
		 */
		if (stage >=1 && ending == false) timer += Time.deltaTime;

		/* After the timer reaches certain point + pre-
		 * randomized time, Ringmaster will play random line
		 */
		if (timer > interval + rand) PlayRingmaster ();

		if (ending == true && endingdone == false) PlayEnding ();
	
	}

	public void PlayEnding(){
		audio.volume = 0.6f;
		audio.clip = endingSpeak;
		audio.Play ();
		endingdone = true;
	}

	public void PlayFalling(){
		pathsArray = new string[2];
		pathsArray[0] = (string) folder4;
		pathsArray[1] = (string) folder5;

		folder = pathsArray[Random.Range (0, pathsArray.Length)];
		
		while (folder == previous) {
			folder = pathsArray[Random.Range (0, pathsArray.Length)];
		}
		previous = folder;
		
		Object[] clips_obj = Resources.LoadAll(path + folder);
		
		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];
			
			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError("Could not locate collection " + name );
		}
		
		audio.clip = clips [Random.Range(0, clips.Length)];
		audio.Play ();
		timer = 0;
	}

	public void PlayRingmaster(){

		/* Stage is 1, when player is still on the groundlevel
		 */
		if (stage == 1) {
			pathsArray = new string[2];
			pathsArray[0] = (string) folder1;
			pathsArray[1] = (string) folder3;
		}

		/* Stage is 2, when player has started to climb.
		 * Adds "Will He Fall" folder to the randomizer
		 */
		if (stage == 2) {
			pathsArray = new string[3];
			pathsArray[0] = (string) folder1;
			pathsArray[1] = (string) folder3;
			pathsArray[2] = (string) folder6;
		}

		/* This is only accessable via trigger, when player
		 * falls into a net. Stage is resumed to 2 after the
		 * fall.
		 */
		if (stage == 3) {
			pathsArray = new string[1];
			pathsArray[0] = (string) folder4;
			stage = 2;
		}

		/* This is only accessable via trigger AND player has
		 * fallen enough times. Stage is resumed to 2
		 * after the fall.
		 */
		if (stage == 4) {
			pathsArray = new string[2]; //should there be only the "tirehtööri not amused" folder?
			pathsArray[0] = (string) folder4;
			pathsArray[1] = (string) folder5;
			stage = 2;
		}

		folder = pathsArray[Random.Range (0, pathsArray.Length)];

		while (folder == previous) {
			folder = pathsArray[Random.Range (0, pathsArray.Length)];
		}
		previous = folder;

		Object[] clips_obj = Resources.LoadAll(path + folder);
		
		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];
			
			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError("Could not locate collection " + name );
		}

		audio.clip = clips [Random.Range(0, clips.Length)];
		audio.Play ();

		rand = Random.Range (0, maxRand);
		timer = 0;
	}

	public void PlayStrength(){
		audio.PlayOneShot (showStrength);
		strength = true;
	}

	public void PlayClimb(){
		audio.clip = kiipeemineAlkaa;
		audio.Play ();
		kiipeily = true;
	}
}
