﻿using UnityEngine;
using System.Collections;

public class Cord : MonoBehaviour {

	public bool Activated = false;
	public GameObject LampLight;
	public GameObject LampOn;
	public GameObject LampOff;

	public GameObject UnconnectedJoint;
	public GameObject MaleSenser;
	public GameObject FemaleSenser;
	public float TriggerDistance = 0.1f;

	private HingeJoint2D PlugJoint;

	// Use this for initialization
	void Start () {
		PlugJoint = UnconnectedJoint.GetComponent<HingeJoint2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!PlugJoint.enabled) {
			if (Vector2.Distance(MaleSenser.transform.position, FemaleSenser.transform.position) < TriggerDistance) {
				PlugJoint.enabled = true;
				Activated = true;
				if (UnconnectedJoint) if (UnconnectedJoint.audio) UnconnectedJoint.audio.Play();
				if (LampLight) if (LampLight.light) LampLight.light.enabled = true;
				if (LampOn && LampOff) {
					LampOff.SetActive(false);
					LampOn.SetActive(true);
				}
			}
		}
	}
}
