﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public GameObject Character;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (Character.transform.position.x, Character.transform.position.y + 1, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (Character.transform.position.x, Character.transform.position.y + 1, transform.position.z);
	}
}
