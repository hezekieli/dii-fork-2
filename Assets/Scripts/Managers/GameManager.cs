﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	// Layer ID:s
	private int layerPlayer = 8;
	private int layerPickupable = 9;
	private int layerAttachable = 10;
	private int layerGround = 11;
	private int layerPlatform = 21;
	private int layerIgnorePlayer = 22;
	private int layerIgnoreAll = 23;		// Might not work, dunno why. Unity...?

	// Use this for initialization
	void Start () {

		/*
		// These in some global "init" script?
		// Ignores all collision for attach limit/middle layer
		for (int i = 0; i < 23; i++) Physics2D.IgnoreLayerCollision(16, i, true);
		for (int i = 0; i < 23; i++) Physics2D.IgnoreLayerCollision(17, i, true);
		for (int i = 0; i < 23; i++) Physics2D.IgnoreLayerCollision(18, i, true);
		for (int i = 0; i < 23; i++) Physics2D.IgnoreLayerCollision(19, i, true);
		Physics2D.IgnoreLayerCollision(16, 17, false);
		Physics2D.IgnoreLayerCollision(18, 19, false);

		// Platform layers
		//for (int i = 24; i < 29; i++) Physics2D.IgnoreLayerCollision(24, i, true);
		
		// IgnoreAll layer
		for (int i = 0; i < 22; i++) Physics2D.IgnoreLayerCollision(23, i, true);
		
		Physics2D.IgnoreLayerCollision(8, 9, true);	// Player - pickupable
		Physics2D.IgnoreLayerCollision(layerPlayer, layerAttachable, true);
		Physics2D.IgnoreLayerCollision(layerAttachable, layerAttachable, true);
		Physics2D.IgnoreLayerCollision (layerPlayer, layerIgnorePlayer, true);


		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
