﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

    public GameObject player;

    public Transform spawnPos2;
    public Transform spawnPos3;
    public Transform spawnPos4;
    public Transform spawnPos5;
    public Transform spawnPos6;
    public GUIText info;
	// Use this for initialization
	void Start () {
        player.transform.position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp("l")) Application.LoadLevel("ScienceDay"); //eli siis L nappi 
        if (Input.GetKeyUp("1"))player.transform.position = transform.position;
        if (Input.GetKeyUp("2")) player.transform.position = spawnPos2.position;
        if (Input.GetKeyUp("3")) player.transform.position = spawnPos3.position;
        if (Input.GetKeyUp("4")) player.transform.position = spawnPos4.position;
        if (Input.GetKeyUp("5")) player.transform.position = spawnPos5.position;
        if (Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("ScienceDayGui"); //eli siis L nappi 
     
	}


    void OnGUI() {
        GUI.Box  (new Rect(5, 5, 200, 100),"");
        GUI.Label(new Rect(10, 5, 200, 100), "- Walk with LB or RB");
        GUI.Label(new Rect(10, 20, 200, 100), "- Move your hands with LS or RS");
        GUI.Label(new Rect(10, 35, 200, 100), "- Grab to things with LT or RT");
    }
}

