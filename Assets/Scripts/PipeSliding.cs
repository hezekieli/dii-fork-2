﻿using UnityEngine;
using System.Collections;

public class PipeSliding : MonoBehaviour {

	public GameObject PipeSliderHandL;
	public GameObject PipeSliderHandR;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject CheckAlternativeAttachObject(GameObject hand) {
		//Debug.Log ("Hand is = " + hand.name);
		if (hand.name == "HandLSlider") {
			PipeSliderHandL.transform.position = new Vector2(PipeSliderHandL.transform.position.x, hand.transform.position.y);
			PipeSliderHandL.rigidbody2D.velocity = new Vector2(0,0);
			PipeSliderHandL.rigidbody2D.angularVelocity = 0;
			return PipeSliderHandL;
		}
		else if (hand.name == "HandRSlider") {
			PipeSliderHandR.transform.position = new Vector2(PipeSliderHandR.transform.position.x, hand.transform.position.y);
			PipeSliderHandR.rigidbody2D.velocity = new Vector2(0,0);
			PipeSliderHandR.rigidbody2D.angularVelocity = 0;
			return PipeSliderHandR;
		}
		else return null;
	}
}
