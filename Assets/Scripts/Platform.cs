﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	public GameObject ColChar;		// Collider Character
	public GameObject ColHandL;		// Collider Hand Left
	public GameObject ColHandR;		// Collider Hand Right
	public GameObject PlatChar;		// Platform Collider Character
	public GameObject PlatHandL;	// Platform Collider Hand Left
	public GameObject PlatHandR;	// Platform Collider Hand Right

	// Use this for initialization
	void Start () {


		PlatChar.collider2D.enabled = false;
		PlatHandL.collider2D.enabled = false;
		PlatHandR.collider2D.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
