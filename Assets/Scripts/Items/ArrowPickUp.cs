﻿using UnityEngine;
using System.Collections;

public class ArrowPickUp : CustomPickUp
{
    int cooldownCounter = 50;
    bool cooldown;
    bool arrowShotMode;
    bool arrowInHand;
    public override void Start()
    {
        player = GameObject.Find("PlayerCharacter");
        handL = GameObject.Find("playerHandL");
        handR = GameObject.Find("playerHandR");
        controlsScript = player.GetComponent<PlayerControls>();
        arrowPickup = true;
    }

    public override bool PickUp()
    {
        if (controlsScript.GetRightHandItemScript().bowPickup)
        {
            Physics2D.IgnoreLayerCollision(8, 9, true);
            rigidbody2D.gravityScale = 0;
            transform.position = latestHand.position;
            transform.right = -(latestHand.position - handR.transform.position);
            arrowInHand = true;
        }

        if (controlsScript.GetLeftHandItemScript().bowPickup)
        {
            Physics2D.IgnoreLayerCollision(8, 9, true);
            rigidbody2D.gravityScale = 0;
            transform.position = latestHand.position;
            transform.right = -(latestHand.position - handL.transform.position);
            arrowInHand = true;
        }
        else StandardOneHandPickup();
        return false;
    }

    void FixedUpdate()
    {
        if (controlsScript.GetLeftHandObj() == null && arrowInHand && controlsScript.GetRightHandItemScript().bowPickup)
        {
            ShootArrow();
            arrowInHand = false;
            arrowShotMode = true;
        }
        if (controlsScript.GetRightHandObj() == null && arrowInHand && controlsScript.GetLeftHandItemScript().bowPickup)
        {
            ShootArrow();
            arrowInHand = false;
            arrowShotMode = true;
        }

        if (cooldown) cooldownCounter--;
        if (cooldownCounter == 0)
        {
            cooldown = false;
            cooldownCounter = 50;
        }
        leftHand = null;
        rightHand = null;
    }

    void ShootArrow()
    {
        if (controlsScript.GetRightHandItemScript().bowPickup) rigidbody2D.AddForce((handR.transform.position - handL.transform.position) * 400f);
        if (controlsScript.GetLeftHandItemScript().bowPickup) rigidbody2D.AddForce((handL.transform.position - handR.transform.position) * 400f);
        Physics2D.IgnoreLayerCollision(8, 14, true);
        Physics2D.IgnoreLayerCollision(9, 14, true);
        gameObject.layer = 14;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (arrowShotMode && collision.collider.gameObject.layer != 15)
        {
            gameObject.layer = 0;
            rigidbody2D.isKinematic = true;
            Instantiate(arrowAttach, arrowAttachPos.position, arrowAttachPos.rotation);
            arrowShotMode = false;
        }
        if (arrowShotMode && collision.collider.gameObject.layer == 15)
        {
            Destroy(gameObject);
        }
    }
}