﻿using UnityEngine;
using System.Collections;

public class BlackFader : MonoBehaviour {

    public bool fadeToAlpha;
    public bool fadeToColor = false;
    public bool fadedToColor = false;
    public bool fadedToAlpha = false;
    public float fadeInSpeed = 0.8f;
    public float fadeOutSpeed = 0.8f;

	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (fadeToAlpha)
        {
            guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeOutSpeed * Time.deltaTime);

            if (guiTexture.color.a <= 0.05f)
            {
                // ... set the colour to clear and disable the GUITexture.
                guiTexture.color = Color.clear;
                guiTexture.enabled = false;
                fadeToAlpha = false;
                fadedToAlpha = true;
            }
        }
        else fadedToAlpha = false;

        if (fadeToColor)
        {
            guiTexture.enabled = true;
            guiTexture.color = Color.Lerp(guiTexture.color, Color.white, fadeInSpeed * Time.deltaTime);
            if (guiTexture.color.a >= 0.95f)
            {
                fadeToColor = false;
                fadedToColor = true;
            }
        }
        else fadedToColor = false;
	}
}
