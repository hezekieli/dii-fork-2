﻿using UnityEngine;
using System.Collections;

public class musicHandler : MonoBehaviour {
	
	public string looppiKansio1;
	public string looppiKansio2;
	public AudioClip introPuhe;
	private string looppiPolku = "Sounds/music/";
	public int nykyneVaihe;
	public string nykyneLoop;
	public AudioClip[] clips;
	public int nextClip;

	public bool playedIntro;
	public bool introStarted;

	public bool ending;
	public bool endingdone;
	public AudioClip endingMusic;

	private int edellineVaihe;

	public GameObject tutomanageri;


	
	// Use this for initialization
	void Start () {
		audio.loop = false;
		playedIntro = false;
		introStarted = false;
		ending = false;
		endingdone = false;
		nykyneVaihe = 1;
		edellineVaihe = nykyneVaihe;
		nykyneLoop = looppiKansio1;


		nextClip = 0;

		//"Sounds/music/Tutorial/ekaloopit/1_1.wav"
		//looppiPolku + nykyneLoop
			
		Object[] clips_obj = Resources.LoadAll(looppiPolku + nykyneLoop);
		
		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];
			
			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError("Could not locate collection " + name );
		}
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (nykyneVaihe != edellineVaihe) VaihdaLoop ();

		if (!audio.isPlaying && introStarted == false) {
			introStarted = true;
			audio.clip = introPuhe;
			audio.Play();
		}

		if (!audio.isPlaying && introStarted == true && playedIntro == false) {
			tutomanageri.GetComponent<TutoManager> ().task = 1;
			playedIntro = true;

		}
		if (!audio.isPlaying && playedIntro == true && ending == false) PlayLoop ();

		if (ending == true && endingdone == false) PlayEnding ();
		
	}

	public void PlayEnding (){
		audio.Stop ();
		audio.volume = 0.25f;
		audio.clip = endingMusic;
		audio.Play ();
		endingdone = true;
	}

	public void VaihdaLoop (){
		if (nykyneVaihe == 1) nykyneLoop = looppiKansio1;
		if (nykyneVaihe == 2) nykyneLoop = looppiKansio2;

		Object[] clips_obj = Resources.LoadAll(looppiPolku + nykyneLoop);

		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];
			
			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError("Could not locate collection " + name );
		}

		edellineVaihe = nykyneVaihe;
		nextClip = 0;

	}

	public void PlayLoop(){

		if (nextClip >= clips.Length){
			nextClip = 0;
		}
			
		audio.clip = (clips[nextClip]);
		audio.Play ();
		nextClip++;
	}
}
