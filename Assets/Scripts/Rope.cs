﻿using UnityEngine;
using System.Collections;

public class Rope : MonoBehaviour {
	
	public bool OpenEnd;
	public GameObject RopeEnd;
	public int RopePieces;			// If open ended
	public int ExtraPieces;			// If not open ended
	public GameObject RopePiece;	// What prefab to use for the 
	public float RopePieceLength;


	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
