﻿using UnityEngine;
using System.Collections;

public class LightOnOff : MonoBehaviour {

	public GameObject LampOff;
	public GameObject LampOn;

	private bool lightOn = false;

	public bool LightOn {
		get { return lightOn; }
		set {
			lightOn = value;
			if (lightOn) {
				LampOff.SetActive(false);
				LampOn.SetActive(true);
			}
			else {
				LampOff.SetActive(true);
				LampOn.SetActive(false);
			}
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
