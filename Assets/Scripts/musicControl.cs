﻿using UnityEngine;
using System.Collections;

public class musicControl : MonoBehaviour {

	public AudioClip endmusic;
	public AudioClip mainloop;
	public bool playedIntro;
	public bool ending;
	public bool playedAlready;

	// Use this for initialization
	void Start () {
		audio.loop = false;
		playedIntro = false;
		ending = false;
		playedAlready = false;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (playedIntro == false && !audio.isPlaying) {
			audio.volume = 0.25f;
			audio.clip = mainloop;
			audio.loop = true;
			audio.Play();
			playedIntro = true;
		}

		if (ending == true && playedAlready == false) {
			audio.loop = false;
			if (!audio.isPlaying){
				audio.clip = endmusic;
				audio.Play();
				playedAlready = true;
			}
		}
	
	}
}
