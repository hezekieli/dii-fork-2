﻿using UnityEngine;
using System.Collections;

public class NetVerticalRope : MonoBehaviour {

	public GameObject NetBottom;
	public GameObject NetVisual;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = NetBottom.transform.position;
		transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2 (
			NetVisual.transform.position.x - NetBottom.transform.position.x,
			NetVisual.transform.position.y - NetBottom.transform.position.y) / Mathf.PI * 180);
		transform.localScale = new Vector2 (1, Vector2.Distance (NetBottom.transform.position, NetVisual.transform.position));
	}
}
