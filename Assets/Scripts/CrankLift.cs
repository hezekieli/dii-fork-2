﻿using UnityEngine;
using System.Collections;

public class CrankLift : MonoBehaviour {

	//public GameObject CrankObject;
	//private Crank crank;
	private SliderJoint2D LiftJoint;
	private JointMotor2D LiftMotor;
	public float motorSpeedMax = 50f;

	private float elevationMax = 10f;
	private float elevationMin = 0f;
	private float elevationTarget = 0f;

	private bool liftUp;

	public GameObject Lamp;
	private LightOnOff lightSwitch;

	public bool LiftUp {
		get { return liftUp; }
	}

	/**
	 * @value clamped between 0 (Min) and 1 (Max)
	 */
	public float ElevationTarget {
		get {
			return (elevationTarget - elevationMin) / (elevationMax - elevationMin);
		}
		set {
			//Debug.Log (value);
			elevationTarget = elevationMin + Mathf.Clamp (value, 0, 1) * (elevationMax - elevationMin);
		}
	}

	// Use this for initialization
	void Start () {

		//crank = CrankObject.GetComponent<Crank> ();
		if (transform.localPosition.y < elevationMax - 0.1f) liftUp = true;
		else liftUp = false;

		LiftJoint = GetComponent<SliderJoint2D> ();
		LiftMotor = LiftJoint.motor;

		elevationMax = LiftJoint.limits.max;
		elevationMin = LiftJoint.limits.min;

		lightSwitch = Lamp.GetComponent<LightOnOff> ();
	}
		


	public void setMotorSpeed(float value) {
		LiftMotor.motorSpeed = Mathf.Clamp(value, -motorSpeedMax, motorSpeedMax);
	}

	// Update is called once per frame
	void Update () {

		//float elevationTarget = elevationMin + ((elevationMax - elevationMin) * crank.ValueProgress / (crank.valueMax - crank.valueMin));
		//Debug.Log ("Progress->target " + crank.ValueProgress + " -> " + elevationTarget + " (CurrentY: " + transform.localPosition.y);

		if (transform.localPosition.y >= elevationMax - 0.1f) {
			liftUp = true;
			lightSwitch.LightOn = true;
		}
		else {
			liftUp = true;
			lightSwitch.LightOn = false;
		}

		if (Mathf.Abs(transform.localPosition.y - elevationTarget) > 0.03f) {

			if (transform.localPosition.y < elevationMax && transform.localPosition.y < elevationTarget) {
				LiftMotor.motorSpeed = 50f;
			}
			else if (transform.localPosition.y > elevationMin && transform.localPosition.y > elevationTarget) {
				LiftMotor.motorSpeed = -50f;
			}
		}
		else {
			LiftMotor.motorSpeed = 0f;
			transform.localPosition = new Vector2(transform.localPosition.x, elevationTarget);
		}

		LiftJoint.motor = LiftMotor;
		//LiftJoint.useMotor = true;

	}
}
